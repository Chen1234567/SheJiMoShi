package Text;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import heap.Data;
import heap.Heap;

public class HeapTest {
    Heap<Integer> heap;
    @Before
    public void setUp() throws Exception {
	Data<Integer>[] array=new Data[11];
	for(int i=0;i<array.length;i++){
	    array[i]=new Data<Integer>(i,i);
	}
	heap=new Heap<Integer>(array);
    }
    @After
    public void teardown() {
	System.out.println("size:"+heap.getSize());
	heap.display();
    }
    @Test
    public final void textSort() {
	Data[] data_sort=heap.sort();
	for(int i=0;i<data_sort.length;i++) {
	    System.out.println(data_sort[i].getData());
	}
	}
    @Test
    public final void testMakeEmpty() {
	heap.makeEmpty();
	System.out.println("makeEmpty");
    }

    @Test
    public final void testInsert() {
	heap.display();
	heap.insert(12, 12);
	System.out.println("insert");
	assertEquals(new Data<Integer>(12, 12).getData(), heap.getTop().getData());
    }

    @Test
    public final void testPopTop() {
	heap.popTop();
	assertEquals(new Data<Integer>(9,9).getData(), heap.getTop().getData());
	System.out.println("popTop");
    }

}
