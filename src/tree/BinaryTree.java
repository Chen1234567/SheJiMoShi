package tree;

import java.util.LinkedList;

public class BinaryTree<T> {
    Node<T> root ;
    public void display() {
	IteratorTree iteratorTree=new IteratorTree<T>(this);
	System.out.println("------无递归,先序");
	iteratorTree.preOrder_noRecursion();
	System.out.println("------无递归,中序");
	iteratorTree.inOrder_noRecursion();
	System.out.println("------无递归,后序");
	iteratorTree.postOrder_noRecursion();
	System.out.println("--------over!");
	
    }
    public BinaryTree() {
	root=null;
    }
    
    public Node<T> search(T data) throws NoSuchDataException {
	if(root==null) {
	    System.err.println("此二叉树为空");
	    throw new NoSuchDataException();
	}
	Node<T> node_temp=search(root,data );
	if(node_temp!=null) {
	    return node_temp;
	}else {
	    throw new NoSuchDataException();
	}
    }
    private Node search(Node<T> current,T data) {
	Node<T> node;
	    if(current!=null&&current.data==data) {// 找到了就返回
		return current;
	    }
	    if(current!=null&&((node=search(current.leftnode, data))!=null||// 左子树找到了
		    (node=search(current.rightnode,data))!=null)) {// 右子树找到了
		return node;
	    }else {//没找到返回null
		return null;
	    }
    }
    /**<pre>
     * 找父节点 
     * </pre>
     * @throws NoSuchDataException 
     */
    public Node<T> parent(Node<T> search_node) throws NoSuchDataException {
	// 跟节点无父节点
	if (search_node == root) {
	    System.err.println("此节点为跟节点,无父节点");
	    throw new NoSuchDataException();
	}
	return parret(root, search_node);
    }

    private Node<T> parret(Node<T> node, Node<T> search_node) throws NoSuchDataException {
	if (node == null) {// 为空结束
	    return null;
	}
	// 找到就返回此节点
	if (node.leftnode == search_node || node.rightnode == search_node) {
	    return node;
	}
	Node<T> node_temp;
	// 找不到,递归
	if ((node_temp = parent(node.leftnode)) != null) {
	    return node_temp;
	} else {
	    // 左子树没有,必然在右子树
	    return parent(node.rightnode);
	}
    }

    // 判空
    public boolean isEmpty() {
	if (root == null) {
	    return true;
	}
	return false;
    }

    /**<pre>  前序遍历 
     *  遍历树,输出node.toString()+"----------已visit"
     * </pre>
     */
    public void visit() {
	visit(root);
	System.out.println("------visit over");
    }

    private void visit(Node<T> node) {
	if (node != null) {
	    System.out.println(node.data+"----------已visit");
	    visit(node.leftnode);
	    visit(node.rightnode);
	}
	return;
    }

    /**<pre>
     * 获得树的节点个数 
     *</pre>
     */
    public int size() {
	// 树的节点多少
	if (root == null) {
	    return 0;
	}
	return size(root);
    }

    // 递归-----size
    private int size(Node<T> node) {
	if (node == null) {
	    return 0;
	}
	int i = size(node.leftnode);
	int j = size(node.rightnode);
	return i + j + 1;
    }

    /**<pre>
     *  获得树的高度 
     *  </pre>
     */
    public int get_height() {
	return height(root);
    }

    private int height(Node<T> node) {
	if (node == null) {
	    // 空树高度为0
	    return 0;
	}
	int left_tree_num = height(node.leftnode);
	int right_tree_num = height(node.rightnode);
	if (left_tree_num >= right_tree_num) {
	    return left_tree_num + 1;
	} else {
	    return right_tree_num + 1;
	}
    }
    

    private int index=0;
    private LinkedList<Node<T>> tree;
    /**<pre>
     * @param <E>
     *  使用先序储存
     *  </pre>
     */
    public void  input(LinkedList<Node<T>> tree) {
	if(tree.isEmpty()) {
	    System.out.println("输入为空!");
	    return ;
	}
	this.tree=tree;
	root=creat(root);
    }
    private  Node<T> creat(Node<T> node_current) {// 传进来,根节点
	if(index==(tree.size()-1)) {
	    return node_current; //返回根节点,递归结束
	}
	if(tree.get(index)==null) {// 若为空,跳过
	    index++;
	    node_current=null;
	    return null;
	}
	node_current=tree.get(index);//创建根节点
	index++;// 创建一次+1 
	node_current.leftnode=creat(node_current.leftnode);// 左子树递归
	node_current.rightnode=creat(node_current.rightnode);// 右子树递归
	return node_current;
    }
    /**<pre>
     * 删除树(子树) 
     * </pre>
     */
    public Node<T> free_tree(Node<T> node) {// TODO 不完善,本节点还留着
	
	  // 左右子树都释放后,才释放根节点---后序遍历
	if (node != null) {
	    node.leftnode=free_tree(node.leftnode);
	    node.rightnode=free_tree(node.rightnode);
	    return null;
	}else {
	    return null;
	}
    }
    public Node makeEmpty() {
	this.root=null;
	return this.root;
    }
}
