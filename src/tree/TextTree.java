package tree;

public class TextTree extends SortBinaryTree {
	private void insert (Integer x, Node<Integer> t) {
	  //递归的二叉搜索树插入算法
	     if ( t == null)           //空二叉树
	         t = new Node<Integer>(x);     //创建含 x 结点
	     int result = x.compareTo(t.data);    
	     if (result<0)          //在左子树插入
	        insert( x, t.leftnode);
	     else if (result>0)         //在右子树插入
	        insert(x, t.rightnode);
	     else return; //Duplicate; do nothing
	  }
	  public void insertText(Integer x)
	  { insert(x,root); }


    }
