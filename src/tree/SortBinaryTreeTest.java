package tree;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

public class SortBinaryTreeTest {
    SortBinaryTree sortBinaryTree=new SortBinaryTree();
    @Before
    public void setUp() throws Exception {
	LinkedList<Node<Integer>> tree=new LinkedList<Node<Integer>>();
	tree.add(new Node<Integer>(new Integer(5)));
	tree.add(new Node<Integer>(new Integer(3)));
	tree.add(new Node<Integer>(new Integer(4)));
	tree.add(new Node<Integer>(new Integer(2)));
	tree.add(new Node<Integer>(new Integer(3)));
	tree.add(new Node<Integer>(new Integer(8)));
	tree.add(new Node<Integer>(new Integer(0)));
	tree.add(new Node<Integer>(new Integer(9)));
	tree.add(new Node<Integer>(new Integer(1)));
	tree.add(new Node<Integer>(new Integer(7)));
	tree.add(new Node<Integer>(new Integer(89)));
	tree.add(new Node<Integer>(new Integer(34)));
	tree.add(new Node<Integer>(new Integer(12)));
	for(int i=0;i<tree.size();i++) {
	    sortBinaryTree.insert(tree.get(i).data);
	}
    }

    @Test
    public final void testSearchSortInteger() throws NoSuchDataException {
	assertEquals(9, sortBinaryTree.searchSort(9).data.intValue());
    }

    @Test
    public final void testFind() throws NoSuchDataException {
	assertEquals(89, sortBinaryTree.find(89).data.intValue());
    }

    @Test
    public final void testFindMax() {
	try {
	    assertEquals(89, sortBinaryTree.findMax().intValue());
	} catch (NoSuchDataException e) {
	    // TODO 自动生成的 catch 块
	    e.printStackTrace();
	}
    }

    @Test
    public final void testFindMin() {
	try {
	    assertEquals(0, sortBinaryTree.findMin().intValue());
	} catch (NoSuchDataException e) {
	    // TODO 自动生成的 catch 块
	    e.printStackTrace();
	}
    }

    @Test
    public final void testInsert() throws HasStoreException {
	sortBinaryTree.insert(-1);
	try {
	    assertEquals(-1, sortBinaryTree.findMin().intValue());
	} catch (NoSuchDataException e) {
	    // TODO 自动生成的 catch 块
	    e.printStackTrace();
	}
    }

}
