package tree;

import static org.junit.Assert.*;

import java.util.LinkedList;

import org.junit.Before;
import org.junit.Test;

public class TextTreeTest {
    TextTree text=new TextTree();
    @Before
    public void setUp() throws Exception {
	LinkedList<Node<Integer>> tree=new LinkedList<Node<Integer>>();
	tree.add(new Node<Integer>(new Integer(5)));
	tree.add(new Node<Integer>(new Integer(3)));
	tree.add(new Node<Integer>(new Integer(4)));
	tree.add(new Node<Integer>(new Integer(2)));
	tree.add(new Node<Integer>(new Integer(3)));
	tree.add(new Node<Integer>(new Integer(8)));
	tree.add(new Node<Integer>(new Integer(0)));
	tree.add(new Node<Integer>(new Integer(9)));
	tree.add(new Node<Integer>(new Integer(1)));
	tree.add(new Node<Integer>(new Integer(7)));
	tree.add(new Node<Integer>(new Integer(89)));
	tree.add(new Node<Integer>(new Integer(34)));
	tree.add(new Node<Integer>(new Integer(12)));
	for(int i=0;i<tree.size();i++) {
	    text.insertText(tree.get(i).data);
	}
    }

    @Test
    public final void testInsertText() throws NoSuchDataException {
	assertEquals(null, text.root);
    }

}
