package tree;

import java.util.Stack;

public  class IteratorTree<T>  {
    private BinaryTree binaryTree;
    private Node<T> current;
    public IteratorTree(BinaryTree<T> binaryTree) {
	this.binaryTree=binaryTree;
	current=binaryTree.root;
    }
    
    public  void visit(Node<T> node) {
	// TODO 可改
	System.out.println("------"+node.data);
    }
    /**<pre>
     * @功能  使用非递归算法----中序遍历
     * </pre>
     */
    public void inOrder_noRecursion() {
	current=binaryTree.root;
	Stack<Node<T>> s=new Stack<Node<T>>();
	while(!s.isEmpty()||current!=null) {
	    while(current!=null) {
		    s.push(current);
		    current=current.leftnode;
		    if(current==null) {//只要左节点存在,循环
			break;
		    }
		}
	    if(!s.isEmpty()) {
		visit(s.peek());
		current=s.pop().rightnode;
	    }
	}
    }
    
    /**<pre>
     * @功能 非递归实现先序遍历
     * </pre>  
     */
    public void preOrder_noRecursion() {
	Stack<Node<T>> s=new Stack<Node<T>>();
	while(!s.isEmpty()||current!=null) {
	    while(current!=null) {
		visit(current);
		if(current.rightnode!=null) {
		    s.push(current.rightnode);
		}
		current=current.leftnode;
	    }
	    if(!s.isEmpty()) {
		current=s.pop();
	    }
	}
    }
	
    /**<pre>
     * 非递归实现后序遍历 
     * </pre>
     */
    public void postOrder_noRecursion() {
	Stack<Node<T>> s=new Stack<Node<T>>();
	Node<T> current=binaryTree.root;
	Node<T> lastvisit=null;
	while(current!=null) {//左访问到头
	    s.push(current);// 第一次访问,压入
	    current=current.leftnode;
	}
	while(!s.isEmpty()) {// 后序遍历,栈为空就结束了
	    current=s.pop();//栈的头结点第二次遇见,弹出
		// 当前节点的右节点没有访问过
		if(current.rightnode!=null&&current.rightnode!=lastvisit) {
		    s.push(current);// 右节点未遍历,current节点压入
		    current=current.rightnode;// 去访问右子树
		    while(current!=null) {//进入右节点后左访问到头
			s.push(current);
			current=current.leftnode;
		    }
		}else {//当前节点的右节点已经访问过
		    visit(current);
		    lastvisit=current;// 访问过后,当前节点变上次访问的节点
		}
	}
    }
}
