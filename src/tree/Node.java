package tree;


public class Node<T> {
	T data = null;
	Node<T> leftnode = null;
	Node<T> rightnode = null;

	public Node() {
	}

	public Node(T data) {
	    this.data = data;
	}
	public Node(T data,Node<T> lNode,Node<T> rNode) {
	    this.data=data;
	    this.leftnode=lNode;
	    this.rightnode=rNode;
	}
   }