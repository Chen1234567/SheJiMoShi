package tree;

public class HasStoreException extends Exception{
    public HasStoreException() {
	super("已存储过此数据!");
    }
}
