package tree;

/**
 * 实现二分树查找
 * @author 26306
 * @param <T>
 * @param <T>
 *
 */
public class SortBinaryTree extends BinaryTree<Integer>{
//      使用递归
    public Node<Integer> searchSort(Integer data) throws NoSuchDataException {
	Node<Integer> search_node=search(data,this.root);
	if(search_node==null) {
	    throw new NoSuchDataException();
	}
	return search_node;
    }
    private Node search(Integer search_data,Node<Integer> root_node) {
	if(root_node==null) {//找不到,返回空
	    return null;
	}
	if(search_data.intValue()==root_node.data.intValue()) {
	    return root_node;
	}if(search_data>root_node.data) {//要搜索的数据在右子树上
	    return search(search_data, root_node.rightnode);
	}else {//在左子树上
	    return search(search_data, root_node.leftnode);
	}
    }
    /**使用迭代
     * @param search_data
     * @return
     * @throws NoSuchDataException 
     */
    public Node<Integer> find(Integer search_data) throws NoSuchDataException{
	Node<Integer> current_node=this.root;
	while(current_node!=null) {
	    if(current_node.data>search_data) {//搜索的数据在左子树上
		current_node=current_node.leftnode;
	    }else if(current_node.data<search_data) {//搜索的数据在右子树上
		current_node=current_node.rightnode;
	    }else {
		return current_node;
	    }
	}//退出循环了,找不到了,搜索失败
	throw new NoSuchDataException();
    }
    public Integer findMax()throws NoSuchDataException{
	if(this.isEmpty()) {
	    throw new NoSuchDataException();
	}else {
	    Node<Integer> current_node=this.root;
	   while(current_node.rightnode!=null) {
	       current_node=current_node.rightnode;
	   }
	   return current_node.data;//最右下
	}
    }
    public Integer findMin() throws NoSuchDataException{
	if(this.isEmpty()) {
	    throw new NoSuchDataException();
	}else {
	    Node<Integer> cuNode=this.root;
	    while(cuNode.leftnode!=null) {
		cuNode=cuNode.leftnode;
	    }return cuNode.data;
	}
    }
    public Node<Integer> insert(Integer data_insert) {
	if(this.root==null) {
	    this.root=new Node<Integer>(data_insert);
	    return this.root;
	}else if(data_insert.intValue()>this.root.data.intValue()){
	    try {
		this.root.rightnode=insert(data_insert,this.root.rightnode);
	    } catch (HasStoreException e) {
		// TODO 自动生成的 catch 块
		e.printStackTrace();
	    }//存右树,返回右节点
	    finally{
		return this.root;
	    }
	}else if(data_insert.intValue()<this.root.data.intValue()) {
	    try {
		this.root.leftnode=insert(data_insert, this.root.leftnode);
	    } catch (HasStoreException e) {
		// TODO 自动生成的 catch 块
		e.printStackTrace();
	    }//存左树,返回左节点
	    finally{
		return this.root;
	    }
	}else {//相等
	    try {
		throw new HasStoreException();
	    } catch (HasStoreException e) {
		// TODO 自动生成的 catch 块
		e.printStackTrace();
	    }finally {
		return this.root;
	    }
	}
    }
    //递归实现,返回cu_node
    private Node<Integer> insert(Integer data_insert,Node<Integer> cu_root) throws HasStoreException {
	if(cu_root==null) {
	    cu_root=new Node<Integer>(data_insert);
	    return cu_root;//插入成功
	}
	if(data_insert>cu_root.data) {//要插在右节点上
	    cu_root.rightnode=insert(data_insert,cu_root.rightnode);
	    return cu_root;
	}else if(data_insert<cu_root.data) {//要插在左节点上
	    cu_root.leftnode=insert(data_insert, cu_root.leftnode);
	    return cu_root;
	}else {//重复了,不插入
	    throw new HasStoreException();
	}
    }
}

class sortNode{
    private Node<Integer> sortNode;
    public sortNode(Integer data) {
	sortNode=new Node<Integer>(data);
    }
}