package heap;


/**
 * 建立大根堆
 * 
 * @author 26306
 * @param <Data>
 */
public class Heap<T> {
    private final static int DEFFORT_SIZE = 10;// 默认大小
    private int size;// 堆的节点个数
    public int getSize() {
	return size;
    }
    private Data<T>[] heap;// 数组实现的堆
    public boolean isEmpty() {// 判空
	return size == 0;
    }
    public Data<T> getHeapData(int index){
	return heap[index];
    }
    public boolean isFull() {// 判满
	return size == heap.length;
    }

    public void makeEmpty() {// 置满
	heap=null;
	this.size = 0;
    }

    public Heap(int max_size) {
	if (max_size < DEFFORT_SIZE) {// 堆的max_size最小10
	    heap = new Data[DEFFORT_SIZE];
	} else {
	    heap = new Data[DEFFORT_SIZE*2];
	}
	size = 0;
    }

    public Heap(Data<T>[] array) {// 传入一个数组,由此建立堆
	this(array.length);
	
	for(int i=0;i<array.length;i++) {
	    heap[i]=array[i];//数组传送
	    this.size++;
	}
	int endOfHeap=array.length-1;
	int currentNoleaf_index=(endOfHeap-1)/2;//最后的非叶子节点
	while(currentNoleaf_index>=0) {
	    filterDown(currentNoleaf_index, endOfHeap);
	    currentNoleaf_index--;//上一个叶子节点
	}//由小堆到大堆,时间复杂度为O(nlog(n))
    }

    public void insert(T data, int key) {
	if (isFull()) {
	    System.err.println("满了,不能insert");
	    return;
	}if(isEmpty()) {
	    heap[0]=new Data<T>(data,key);
	    size++;
	    return;
	}else {
	    heap[size] = new Data<T>(data, key);// 在最后一个位置插入
	    filterUp(size);//从最后一个开始往上调整为堆
	    size++;
	}

    }

    //从上往下调整成堆
    private void filterDown(int start,int endOfHeap) {
	if(start==endOfHeap) {//只有一个节点
	    return;
	}
	int parent_index=start;
	int Child_index=parent_index*2+1;//暂为左孩子节点
	Data<T> temp=heap[start];//存开始的那个节点
	while(Child_index<=endOfHeap) {//向下找到比temp低的节点
	    if(Child_index<endOfHeap&&   //比size小一,有右节点
		    heap[Child_index].getKey()<heap[Child_index+1].getKey()) {//两子女中选优先级高的
		Child_index++;//左节点进位,右节点
	    }if(heap[parent_index].getKey()<temp.getKey()) {//如果父亲节点优先级已近比temp节点低了
		break;//传出child_index
	    }else {
		heap[parent_index]=heap[Child_index];//父亲节点被孩子节点覆盖
		parent_index=Child_index;//父亲节点下移
		Child_index=Child_index*2+1;//孩子节点下移(左)
	    }
	}heap[parent_index]=temp;//原本的parent就比temp低,高的代替低的
    }
    
    //从待插入节点向上调整为堆
    private void filterUp(int inserted_index) {
	int child_index=inserted_index;//以待插入节点为初始孩子节点
	int parent_index=(child_index-1)/2;//以待插入节点的父节点为初始父节点
	Data<T> temp=heap[inserted_index];//存储待插入的节点
	while(parent_index>0) {
	    if(heap[parent_index].getKey()>temp.getKey()) {//此父节点大于待插入的节点
		break;
	    }else {
		heap[child_index]=heap[parent_index];//子节点存父亲节点,父亲节点下移
		//父亲节点和孩子节点的index都往上遍历
		child_index=parent_index;
		parent_index= (parent_index-1)/2;
	    }
	}//跳出循环时,该父亲节点刚好大于temp节点,传出孩子节点
	//或者是parent_index==0
	if(heap[0].getKey()<temp.getKey()) {
	    heap[child_index]=heap[0];//top节点下移
	    heap[0]=temp;
	    return;
	}
	heap[child_index]=temp;
    }
    public Data<T> getHeap(int index) {
	return heap[index];
    }
    public Data<T> getTop() {
	if(size==0) {
	    throw new IndexOutOfBoundsException("该heap为空");
	}
	return heap[0];
    }

    public Data<T> popTop() {
	if(size==0) {
	    System.err.println("该heap为空,没有头结点");
	    return null;
	}
	if(size==1) {
	    Data<T> top=heap[0];
	    heap[size-1]=null;
	    size--;
	    return top;
	}
	Data<T> top = heap[0];
	heap[0]=heap[size-1];//最后一个放在堆首
	heap[size-1]=null;//原位置的heap[size-1]就不要了
	size--;
	filterDown(0, size-1);
	return top;
    }

    public void display() {
	if(heap==null) {
	    System.out.println("heap为空");
	    return;
	}
	for(int i=0;i<size;i++){
	    System.out.print(heap[i].getData()+" ");
	}
	System.out.println();
	System.out.println("----------------");
    }
    public Data[] sort() {
	Data<T>[] heap_sort=new Data[this.size];
	int i=0;
	System.out.println("堆排序:");
	while(!this.isEmpty()){
	    heap_sort[i]=this.popTop();
	    i++;
	}
	return heap_sort;
    }
}
